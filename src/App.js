import React from "react";
import AppHeader from "./components/AppHeader";
import AppHero from "./components/AppHero";
import AppAbout from "./components/AppAbout";
import AppServices from "./components/AppServices";
import AppForm from "./components/AppForm";

function App() {
  return (
    <>
      <AppHeader></AppHeader>
      <AppHero></AppHero>
      <AppAbout></AppAbout>
      <AppServices></AppServices>
      <AppForm></AppForm>
    </>
  );
}

export default App;
