import { Card, Container, Row, Col } from "react-bootstrap";

const tab = [1, 2, 3, 4];

function AppServices() {
  return (
    <>
      <Container
        className="mb-5 pb-5 bg-dark d-flex align-items-center"
        style={{ color: "white", minHeight:800 }}
        id="services"
        fluid
      >
        <Container>
          <div>
            <h1 className="mb-5 fs-1 text-center pt-5">Our Services</h1>
          </div>
          <Row className="gap-5">
            <Col xs="12" lg="5" md="12">
              <Row className="d-flex justify-content-center  gap-4">
                {tab.map((el) => (
                  <Card
                    // bg={"dark"}
                    // key={"dark"}
                    // text={"light"}
                    style={{ width: "35%", minWidth:'15em' }}
                  >
                    <Card.Header>{el}</Card.Header>
                    <Card.Body>
                      <Card.Title>Dark Card Title </Card.Title>
                      <Card.Text>
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </Card.Text>
                    </Card.Body>
                  </Card>
                ))}
              </Row>
            </Col>
            <Col xs="12" lg="6" md="12" className="text-center fs-5 py-5 py-lg-0">
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Corporis, consectetur aut ab, minima non labore facilis illo
                dicta exercitationem eum tempore quia minus sunt ratione
                excepturi architecto, tenetur vel quas.
              </p>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Corporis, consectetur aut ab, minima non labore facilis illo
                dicta exercitationem eum tempore quia minus sunt ratione
                excepturi architecto, tenetur vel quas.
              </p>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Corporis, consectetur aut ab, minima non labore facilis illo
                dicta exercitationem eum tempore quia minus sunt ratione
                excepturi architecto, tenetur vel quas.
              </p>
            </Col>
          </Row>
        </Container>
      </Container>
    </>
  );
}

export default AppServices;
