import Carousel from "react-bootstrap/Carousel";

const heroData = [
  {
    id: 1,
    image: require("../assets/images/hero-1.png"),
    title: "Discover The Snake Universe",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis a est doloribus modi ab provident optio. Ipsum a dolores nostrum minima alias? Vitae consequatur quibusdam nesciunt distinctio harum iure molestiae!",
  },
  {
    id: 2,
    image: require("../assets/images/hero-2.png"),
    title: "Do You Love Reptiles ?",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis a est doloribus modi ab provident optio. Ipsum a dolores nostrum minima alias? Vitae consequatur quibusdam nesciunt distinctio harum iure molestiae!",
  },
  {
    id: 3,
    image: require("../assets/images/hero-3.png"),
    title: "Learn More About Snakes",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis a est doloribus modi ab provident optio. Ipsum a dolores nostrum minima alias? Vitae consequatur quibusdam nesciunt distinctio harum iure molestiae!",
  },
];

function AppHero() {
  return (
    <Carousel id="home">
      {heroData.map((item) => (
        <Carousel.Item>
          <img
            src={item.image}
            alt={"slider" + item.id}
            className="img-fluid"
            style={{ width: "100%", height: 700 }}
          />
          <Carousel.Caption>
            <h3>{item.title}</h3>
            <p>{item.description}</p>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
}

export default AppHero;
