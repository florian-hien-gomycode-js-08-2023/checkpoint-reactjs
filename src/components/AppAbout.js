import { Container, Row, Col } from "react-bootstrap";

import AboutImg from "../assets/images/about-1.jpg";

function AppAbout() {
  return (
    <Container
      className="mt-5 d-flex align-items-center justiify-content-center"
      style={{ minHeight: 800 }}
      id="about"
    >
      <div className="about">
        <div>
          <h1 className="mb-5 fs-1 text-center">About Us</h1>
        </div>
        <Row className="d-flex justify-content-center gap-lg-0 gap-5">
          <Col xs="12" lg="4" md="8">
              <img
                src={AboutImg}
                alt=""
                className="about-img"
                style={{ height: "", width: "100% " }}
              />
          </Col>
          <Col xs="12" lg="8" md="12" className="text-center fs-5 pb-5 pb -lg-0">
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit,
              inventore. Ipsam, quasi illo quos autem voluptates labore quas
              libero. Sit voluptates animi nemo officiis dolores ducimus commodi
              modi, aspernatur alias.
            </p>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit,
              inventore. Ipsam, quasi illo quos autem voluptates labore quas
              libero. Sit voluptates animi nemo officiis dolores ducimus commodi
              modi, aspernatur alias.
            </p>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit,
              inventore. Ipsam, quasi illo quos autem voluptates labore quas
              libero. Sit voluptates animi nemo officiis dolores ducimus commodi
              modi, aspernatur alias.
            </p>
          </Col>
        </Row>
      </div>
    </Container>
  );
}

export default AppAbout;
